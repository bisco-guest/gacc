from django.utils.crypto import (
    constant_time_compare, get_random_string, pbkdf2,
)
from django.contrib.auth.hashers import BasePasswordHasher

class LdapCryptPasswordHasher(BasePasswordHasher):
    """
    Password hashing using UNIX crypt - this is basically
    a copy of django.contrib.auth.hashers.CryptPasswordHasher
    but it formats the password hash for use in openldap
    """
    algorithm = "{crypt}"
    library = "crypt"

    def salt(self):
        """
        mksalt:
        19 characters starting with $digit$ and 16 random
        characters from the set [./a-zA-Z0-9], suitable for
        passing as the salt argument to crypt()
        """
        crypt = self._load_library()
        return crypt.mksalt(crypt.METHOD_SHA512)

    def encode(self, password, salt):
        crypt = self._load_library()
        assert len(salt) == 19
        data = crypt.crypt(password, salt)
        assert data is not None  # A platform like OpenBSD with a dummy crypt module.
        # we don't need to store the salt, but Django used to do this
        return "%s%s" % (self.algorithm, data)

    def verify(self, password, encoded):
        crypt = self._load_library()
        algorithm, method, salt, data = encoded.split('$')
        assert algorithm == self.algorithm
        return constant_time_compare("$" + method + "$" + salt + "$" + data, crypt.crypt(password, "$" + method + "$" + salt))

    def safe_summary(self, encoded):
        algorithm, salt, method, data = encoded.split('$', 3)
        assert algorithm == self.algorithm
        return OrderedDict([
            (_('algorithm'), algorithm+method),
            (_('salt'), salt),
            (_('hash'), mask_hash(data, show=3)),
        ])

    def harden_runtime(self, password, encoded):
        pass
