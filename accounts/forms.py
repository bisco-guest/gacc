from django import forms
from django.contrib.auth.forms import UserCreationForm
from .models import LdapUser


class SignUpForm(UserCreationForm):
    username = forms.CharField(max_length=30, help_text='Required.')
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')

    class Meta:
        model = LdapUser
        fields = ('username', 'email', 'password1', 'password2', )
