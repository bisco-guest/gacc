from .models import LdapUser
from django.contrib.auth.hashers import check_password
from django.contrib.auth.models import User

class LdapBackend:
    def authenticate(self, request, username=None, password=None):
        user = LdapUser.objects.get(username=username)
        if check_password(password, user.password):
            try:
                user = User.objects.get(username=username)
            except User.DoesNotExist:
                user = User(username=username)
                user.save()
            return user
        return None

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None
