from django.db import models
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import UserManager
import ldapdb.models
from ldapdb.models.fields import CharField, DateField

class LdapUser(ldapdb.models.Model, AbstractBaseUser):
    """
    Class for representing an LDAP user entry.
    """
    # LDAP meta-data
    base_dn = "ou=people,dc=gsoc,dc=example,dc=org"
    object_classes = ['shadowAccount', 'inetOrgPerson']

    # these fields are needed for the abstractbaseuser
    # registration functionality
    # see https://docs.djangoproject.com/en/2.0/topics/auth/customizing/#specifying-a-custom-user-model
    USERNAME_FIELD = 'username'
    EMAIL_FIELD = 'email'

    # inetOrgPerson
    email = CharField(db_column='mail')

    # posixAccount
    username = CharField(db_column='uid', primary_key=True)
    password = CharField(db_column='userPassword')
    # posixAccount needs sn and cn. we fill them with empty
    # values by default
    sn = CharField("Final name", db_column='sn', default=" ")
    cn = CharField(db_column='cn', default=" ")
    #we are ignoring the last_login field at the moment; the
    #more 'django'-ic solution would probably be to add an
    #attribute in ldap that can store the last_login data
    #NOTE: if we decide to do that, check the formatstring,
    #the Z at the end should probably not be hardcoded
    #last_login = DateField(db_column='PwdLastSuccess', format="%Y%m%d%H%M%SZ")

    objects = UserManager()

    class Meta:
        managed = False

    def __str__(self):
        return self.username

    def __unicode__(self):
        return self.username

    def save(self, *args, **kwargs):
        fields = kwargs.pop('update_fields', [])
        if fields != ['last_login']:
             return super(LdapUser, self).save(*args, **kwargs)
