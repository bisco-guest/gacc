from django.contrib import admin

# Register your models here.
from .models import LdapUser

class LdapUserAdmin(admin.ModelAdmin):
    exclude = ['sn', 'cn', 'last_login']

admin.site.register(LdapUser, LdapUserAdmin)
